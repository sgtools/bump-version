// bump-version - Update project version
// Copyright (C) 2024 Sebastien Guerri
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

use std::path::Path;

use clap::ArgMatches;
use anyhow::anyhow;
use anyhow::Result;
use semver::Version;
use inquire::Select;

use crate::MODES;
use crate::libs::api_rust as rust;
use crate::libs::api_svelte as svelte;

pub fn main_get_version(path: &Path, mode: &str, verbose: bool) -> Result<Version>
{
    let version = match mode {
        "rust" => rust::get_version(&rust::get_file(path)?)?,
        "svelte" => svelte::get_version(&svelte::get_file(path)?)?,
        _ => Err(anyhow!("Invalid mode"))?,
    };
    if verbose { println!("> Current version: {}", version); }
    Ok(version)
}

pub fn main_update_version(path: &Path, mode: &str, version: &Version, verbose: bool) -> Result<()>
{
    match mode {
        "rust" => rust::update_version(&rust::get_file(path)?, version)?,
        "svelte" => svelte::update_version(&svelte::get_file(path)?, version)?,
        _ => Err(anyhow!("Invalid mode"))?,
    }
    if verbose { println!("> Files updated"); }
    Ok(())
}




pub fn get_arg_mode(matches: &ArgMatches, verbose: bool) -> Result<&str>
{
    let mode = match matches.get_one::<String>("type") {
        None => {
            if verbose {
                match Select::new("Mode?", MODES.to_vec()).prompt() {
                    Err(_) => Err(anyhow!("Invalid type argument. Cancelled"))?,
                    Ok(value) => value
                }
            } else {
                Err(anyhow!("Missing type argument"))?
            }
        },
        Some(value) => value
    };
    Ok(mode)
}
