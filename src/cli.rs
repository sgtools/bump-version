// bump-version - Update project version
// Copyright (C) 2024 Sebastien Guerri
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

use clap::Command;
use clap::arg;

use crate::MODES;

pub fn build_cli() -> Command
{
    Command::new(clap::crate_name!())
        .about(clap::crate_description!())
        .version(clap::crate_version!())
        .subcommand_required(true)
        .arg_required_else_help(true)
        .allow_external_subcommands(true)
        .arg(arg!(-v --verbose "Turns on verbose output"))
        .arg(arg!(-t --type <TYPE> "Sets project type").value_parser(MODES))
        .arg(arg!(--gitlab "Updates Gitlab CI/CD"))
        .subcommand(
            Command::new("get")
                .about("Get current version")
        )
        .subcommand(
            Command::new("major")
                .about("Increase major version")
        )
        .subcommand(
            Command::new("minor")
                .about("Increase minor version")
        )
        .subcommand(
            Command::new("patch")
                .about("Increase patch version")
        )
}