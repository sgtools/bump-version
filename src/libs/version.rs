// bump-version - Update project version
// Copyright (C) 2024 Sebastien Guerri
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

use semver::Version;
use semver::Prerelease;
use semver::BuildMetadata;

pub fn increase_major(version: &mut Version, verbose: bool)
{
    version.major += 1;
    version.minor = 0;
    version.patch = 0;
    version.pre = Prerelease::EMPTY;
    version.build = BuildMetadata::EMPTY;
    if verbose { println!("> Version updated: {}", version); }
}

pub fn increase_minor(version: &mut Version, verbose: bool)
{
    version.minor += 1;
    version.patch = 0;
    version.pre = Prerelease::EMPTY;
    version.build = BuildMetadata::EMPTY;
    if verbose { println!("> Version updated: {}", version); }
}

pub fn increase_patch(version: &mut Version, verbose: bool)
{
    version.patch += 1;
    version.pre = Prerelease::EMPTY;
    version.build = BuildMetadata::EMPTY;
    if verbose { println!("> Version updated: {}", version); }
}
