// bump-version - Update project version
// Copyright (C) 2024 Sebastien Guerri
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

use std::path::Path;
use std::path::PathBuf;

use anyhow::anyhow;
use anyhow::Result;
use semver::Version;

pub fn get_file(path: &Path) -> Result<PathBuf>
{
    let mut pb = PathBuf::new();
    pb.push(path);
    pb.push("package.json");
    Ok(pb)
}

pub fn get_version(path: &Path) -> Result<Version>
{
    let content = std::fs::read_to_string(path)?;
    let lines = content.lines();
    for line in lines {
        if let Some((key, value)) = line.trim().split_once(':') {
            if key.trim().to_lowercase().ne("\"version\"") { continue; }
            let version_string = value.trim().replace(",", "");
            let version_string = version_string.trim_matches('\"');
            return Ok(Version::parse(version_string)?);
        }
    }
    Err(anyhow!("No version in package"))?
}

pub fn update_version(path: &Path, version: &Version) -> Result<()>
{
    let mut new_content: Vec<&str> = vec![];
    let new_version = format!("    \"version\":\"{}\",", version);
    let content = std::fs::read_to_string(path)?;
    let lines = content.lines();
    let mut is_done = false;
    for line in lines {
        if let Some((key, _)) = line.trim().split_once(':') {
            if is_done || key.trim().to_lowercase().ne("\"version\"") {
                new_content.push(line);
                continue;
            }
            is_done = true;
            new_content.push(&new_version);
        } else {
            new_content.push(line);
            continue;
        }
    }
    std::fs::write(path, new_content.join("\n"))?;
    Ok(())
}
