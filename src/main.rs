// bump-version - Update project version
// Copyright (C) 2024 Sebastien Guerri
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

use anyhow::Result;

use std::env;

mod cli;
mod utils;
mod libs;
use libs::api_gitlabci as gitlab;

pub const MODES: [&str; 2] = ["rust", "svelte"];

fn main() -> Result<()>
{
    let path = env::current_dir()?;

    let matches = cli::build_cli().get_matches();
    let verbose = matches.get_flag("verbose");
    let has_gitlab = matches.get_flag("gitlab");
    let mode = utils::get_arg_mode(&matches, verbose)?;


    match matches.subcommand() {
        Some(("get", _)) => {
            let version = utils::main_get_version(&path, mode, verbose)?;
            println!("{}", version);
        },
        Some(("major", _)) => {
            let mut version = utils::main_get_version(&path, mode, verbose)?;
            libs::increase_major(&mut version, verbose);
            utils::main_update_version(&path, mode, &version, verbose)?;
            if has_gitlab { gitlab::update_version(&gitlab::get_file(&path)?, &version)?; }
            std::thread::sleep(std::time::Duration::from_secs(1));
            println!("{}", version);
        },
        Some(("minor", _)) => {
            let mut version = utils::main_get_version(&path, mode, verbose)?;
            libs::increase_minor(&mut version, verbose);
            utils::main_update_version(&path, mode, &version, verbose)?;
            if has_gitlab { gitlab::update_version(&gitlab::get_file(&path)?, &version)?; }
            std::thread::sleep(std::time::Duration::from_secs(1));
            println!("{}", version);
        },
        Some(("patch", _)) => {
            let mut version = utils::main_get_version(&path, mode, verbose)?;
            libs::increase_patch(&mut version, verbose);
            utils::main_update_version(&path, mode, &version, verbose)?;
            if has_gitlab { gitlab::update_version(&gitlab::get_file(&path)?, &version)?; }
            std::thread::sleep(std::time::Duration::from_secs(1));
            println!("{}", version);
        },
        _ => unreachable!()
    };

    Ok(())
}
