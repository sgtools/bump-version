# bump-version

[![Gitlab Pipeline Status](https://img.shields.io/gitlab/pipeline-status/sgtools%2Fbump-version?style=flat-square)](https://gitlab.com/sgtools/bump-version/-/pipelines)
[![GitLab Release](https://img.shields.io/gitlab/v/release/sgtools%2Fbump-version?style=flat-square)](https://gitlab.com/sgtools/bump-version/-/releases)
[![Static Badge](https://img.shields.io/badge/license-GPL%203.0-blue?style=flat-square)](https://gitlab.com/sgtools/bump-version/-/blob/main/LICENSE)
[![Static Badge](https://img.shields.io/badge/Open%20Source%3F-Yes!-blue?style=flat-square)](#)


<span style="background-color:red;width:100%;padding:5px;">Project is archived.</span>

See [**rcode**](https://sgtools.gitlab.io/rcode) and `.justfile` in templates.

---

> Update project version

Small tool to update version in my development files  
Based on my workflow, will probably not fit yours

**Main features**
 - rust (Cargo.toml)
 - gitlab CI (.gitlab-ci.yml)

**Roadmap**
 - Will add other languages I use when I will update projects

*Developped and tested on Ubuntu Jammy 22.04*

## Installation (Ubuntu Jammy 22.04)

```bash
sudo sh -c 'curl -SsL https://sgtools.gitlab.io/ppa/pgp-key.public | gpg --dearmor > /etc/apt/trusted.gpg.d/sgtools.gpg'
sudo sh -c 'curl -SsL -o /etc/apt/sources.list.d/sgtools.list https://sgtools.gitlab.io/ppa/sgtools.list'
sudo apt update
sudo apt install bump-version
```

## Usage

```bash
bump-version --help
```

See [documentation page](https://sgtools.gitlab.io/bump-version)

## Dependencies

- [anyhow](https://crates.io/crates/anyhow)
- [clap](https://crates.io/crates/clap)
- [inquire](https://crates.io/crates/inquire)
- [semver](https://crates.io/crates/semver)

## License

Copyright (C) 2024 Sebastien Guerri

This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.

## Contributors

- Sébastien Guerri: [@sguerri](https://gitlab.com/sguerri)