## What's Changed in v0.2.0 
* feat: Add svelte

## What's Changed in v0.1.6 
* chore: Update tasks and gitlabci

## What's Changed in 0.1.5 
* build: Update dependencies
* chore: Update tasks
* docs: Update README
* refactor: Update delay after update
* chore: Update TODO

## What's Changed in 0.1.4 
* docs: Update CI file to correct publishing of docs

## What's Changed in 0.1.3 
* ci: Fix CI file

## What's Changed in 0.1.2 
* chore: Add CHANGELOG

## What's Changed in 0.1.0
* Initial Commit
